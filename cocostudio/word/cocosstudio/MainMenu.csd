<GameFile>
  <PropertyGroup Name="MainMenu" Type="Layer" ID="470ba13a-3a2f-442b-9518-82ae42b6b15e" Version="2.3.3.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="60" Speed="1.0000" ActivedAnimationName="show">
        <Timeline ActionTag="1006502886" Property="Position">
          <PointFrame FrameIndex="0" X="-200.0000" Y="794.4048">
            <EasingData Type="26" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="332.5440" Y="794.4048">
            <EasingData Type="25" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="-200.0000" Y="794.4048">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1006502886" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.7032" Y="1.2606">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.7032" Y="1.2606">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.7032" Y="1.2606">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1006502886" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1385532951" Property="Position">
          <PointFrame FrameIndex="0" X="113.4264" Y="-170.0000">
            <EasingData Type="26" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="113.4264" Y="85.1440">
            <EasingData Type="25" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="113.4264" Y="-170.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1385532951" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.7032" Y="1.2606">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.7032" Y="1.2606">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.7032" Y="1.2606">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1385532951" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-864990817" Property="Position">
          <PointFrame FrameIndex="0" X="524.3224" Y="-170.0000">
            <EasingData Type="26" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="524.3224" Y="74.7968">
            <EasingData Type="25" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="524.3224" Y="-170.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-864990817" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.7032" Y="1.2606">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.7032" Y="1.2606">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.7032" Y="1.2606">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-864990817" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-710455606" Property="Position">
          <PointFrame FrameIndex="0" X="516.9106" Y="1250.0000">
            <EasingData Type="26" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="516.9106" Y="1025.8198">
            <EasingData Type="25" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="516.9106" Y="1250.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-710455606" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.7032" Y="1.2606">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.7032" Y="1.2606">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.7032" Y="1.2606">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-710455606" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="404671481" Property="Position">
          <PointFrame FrameIndex="0" X="139.1718" Y="1250.0000">
            <EasingData Type="26" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="139.1718" Y="1021.5641">
            <EasingData Type="25" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="139.1718" Y="1250.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="404671481" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.7032" Y="1.2606">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.7032" Y="1.2606">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.7032" Y="1.2606">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="404671481" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2002949490" Property="Position">
          <PointFrame FrameIndex="0" X="840.0000" Y="617.8704">
            <EasingData Type="26" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="319.5520" Y="617.8704">
            <EasingData Type="25" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="840.0000" Y="617.8704">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-2002949490" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.7032" Y="1.2606">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.7032" Y="1.2606">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.7032" Y="1.2606">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2002949490" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1994720939" Property="Position">
          <PointFrame FrameIndex="0" X="-200.0000" Y="452.8096">
            <EasingData Type="26" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="315.2000" Y="452.8096">
            <EasingData Type="25" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="-200.0000" Y="452.8096">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-1994720939" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.7032" Y="1.2606">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.7032" Y="1.2606">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.7032" Y="1.2606">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1994720939" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1284145124" Property="Position">
          <PointFrame FrameIndex="0" X="840.0000" Y="267.5280">
            <EasingData Type="26" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="316.6080" Y="267.5280">
            <EasingData Type="25" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="840.0000" Y="267.5280">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1284145124" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.7032" Y="1.2606">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.7032" Y="1.2606">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.7032" Y="1.2606">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1284145124" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="show" StartIndex="0" EndIndex="30">
          <RenderColor A="150" R="240" G="255" B="255" />
        </AnimationInfo>
        <AnimationInfo Name="quit" StartIndex="30" EndIndex="60">
          <RenderColor A="150" R="70" G="130" B="180" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Layer" Tag="30" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="1136.0000" />
        <Children>
          <AbstractNodeData Name="btn_ParcticeMode" ActionTag="1006502886" CallBackType="Click" CallBackName="menuCallback" Tag="31" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-316.6880" RightMargin="716.6880" TopMargin="296.5025" BottomMargin="758.4975" TouchEnable="True" FontSize="24" ButtonText="单词练习" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="210" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="240.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.4862" ScaleY="0.4433" />
            <Position X="-200.0000" Y="794.4048" />
            <Scale ScaleX="0.7032" ScaleY="1.2606" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.3125" Y="0.6993" />
            <PreSize X="0.3750" Y="0.0713" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Normal" Path="BaS32.png" Plist="" />
            <PressedFileData Type="Normal" Path="BaS31.png" Plist="" />
            <NormalFileData Type="Normal" Path="BaS32.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_Setting" ActionTag="1385532951" CallBackType="Click" CallBackName="menuCallback" Tag="33" IconVisible="False" LeftMargin="-6.5736" RightMargin="406.5736" TopMargin="1265.5000" BottomMargin="-210.5000" TouchEnable="True" FontSize="36" ButtonText="设置" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="210" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="240.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="113.4264" Y="-170.0000" />
            <Scale ScaleX="0.7032" ScaleY="1.2606" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1772" Y="-0.1496" />
            <PreSize X="0.3750" Y="0.0713" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Normal" Path="BaS32.png" Plist="" />
            <PressedFileData Type="Normal" Path="BaS31.png" Plist="" />
            <NormalFileData Type="Normal" Path="BaS32.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_Store" ActionTag="-864990817" CallBackType="Click" CallBackName="menuCallback" Tag="34" IconVisible="False" LeftMargin="404.3224" RightMargin="-4.3224" TopMargin="1265.5000" BottomMargin="-210.5000" TouchEnable="True" FontSize="36" ButtonText="商店" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="210" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="240.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="524.3224" Y="-170.0000" />
            <Scale ScaleX="0.7032" ScaleY="1.2606" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8193" Y="-0.1496" />
            <PreSize X="0.3750" Y="0.0713" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Normal" Path="BaS32.png" Plist="" />
            <PressedFileData Type="Normal" Path="BaS31.png" Plist="" />
            <NormalFileData Type="Normal" Path="BaS32.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_Achievement" ActionTag="-710455606" CallBackType="Click" CallBackName="menuCallback" Tag="7" IconVisible="False" LeftMargin="396.9106" RightMargin="3.0894" TopMargin="-154.5000" BottomMargin="1209.5000" TouchEnable="True" FontSize="36" ButtonText="成就" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="210" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="240.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="516.9106" Y="1250.0000" />
            <Scale ScaleX="0.7032" ScaleY="1.2606" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8077" Y="1.1004" />
            <PreSize X="0.3750" Y="0.0713" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Normal" Path="BaS32.png" Plist="" />
            <PressedFileData Type="Normal" Path="BaS31.png" Plist="" />
            <NormalFileData Type="Normal" Path="BaS32.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_Quit" ActionTag="404671481" CallBackType="Click" CallBackName="menuCallback" Tag="8" IconVisible="False" LeftMargin="19.1718" RightMargin="380.8282" TopMargin="-154.5000" BottomMargin="1209.5000" TouchEnable="True" FontSize="36" ButtonText="退出" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="210" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="240.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="139.1718" Y="1250.0000" />
            <Scale ScaleX="0.7032" ScaleY="1.2606" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2175" Y="1.1004" />
            <PreSize X="0.3750" Y="0.0713" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Normal" Path="BaS32.png" Plist="" />
            <PressedFileData Type="Normal" Path="BaS31.png" Plist="" />
            <NormalFileData Type="Normal" Path="BaS32.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_StrongRememberMode" ActionTag="-2002949490" CallBackType="Click" CallBackName="menuCallback" Tag="9" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="723.3120" RightMargin="-323.3120" TopMargin="473.0369" BottomMargin="581.9631" TouchEnable="True" FontSize="24" ButtonText="强力记忆" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="210" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="240.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.4862" ScaleY="0.4433" />
            <Position X="840.0000" Y="617.8704" />
            <Scale ScaleX="0.7032" ScaleY="1.2606" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.3125" Y="0.5439" />
            <PreSize X="0.3750" Y="0.0713" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Normal" Path="BaS32.png" Plist="" />
            <PressedFileData Type="Normal" Path="BaS31.png" Plist="" />
            <NormalFileData Type="Normal" Path="BaS32.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_NoEndMode" ActionTag="-1994720939" CallBackType="Click" CallBackName="menuCallback" Tag="10" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="-316.6880" RightMargin="716.6880" TopMargin="638.0977" BottomMargin="416.9023" TouchEnable="True" FontSize="24" ButtonText="无尽的单词" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="210" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="240.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.4862" ScaleY="0.4433" />
            <Position X="-200.0000" Y="452.8096" />
            <Scale ScaleX="0.7032" ScaleY="1.2606" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.3125" Y="0.3986" />
            <PreSize X="0.3750" Y="0.0713" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Normal" Path="BaS32.png" Plist="" />
            <PressedFileData Type="Normal" Path="BaS31.png" Plist="" />
            <NormalFileData Type="Normal" Path="BaS32.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_LostBridge" ActionTag="1284145124" CallBackType="Click" CallBackName="menuCallback" Tag="11" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="723.3120" RightMargin="-323.3120" TopMargin="823.3793" BottomMargin="231.6207" TouchEnable="True" FontSize="24" ButtonText="失意桥" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="210" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="240.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.4862" ScaleY="0.4433" />
            <Position X="840.0000" Y="267.5280" />
            <Scale ScaleX="0.7032" ScaleY="1.2606" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.3125" Y="0.2355" />
            <PreSize X="0.3750" Y="0.0713" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Normal" Path="BaS32.png" Plist="" />
            <PressedFileData Type="Normal" Path="BaS31.png" Plist="" />
            <NormalFileData Type="Normal" Path="BaS32.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>