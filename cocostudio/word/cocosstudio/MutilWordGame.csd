<GameFile>
  <PropertyGroup Name="MutilWordGame" Type="Layer" ID="69e810ad-9263-4ae7-91ff-43df970a543b" Version="2.3.3.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="60" Speed="1.0000" ActivedAnimationName="wordIn">
        <Timeline ActionTag="1760187276" Property="Position">
          <PointFrame FrameIndex="0" X="-160.0000" Y="568.0000">
            <EasingData Type="26" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="160.0000" Y="568.0000">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="-160.0000" Y="568.0000">
            <EasingData Type="26" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1760187276" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="1.0000" Y="1.0000">
            <EasingData Type="26" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1760187276" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1760187276" Property="FrameEvent">
          <EventFrame FrameIndex="30" Tween="False" Value="leftWordInComplete" />
          <EventFrame FrameIndex="60" Tween="False" Value="leftWordOutComplete" />
        </Timeline>
        <Timeline ActionTag="396031759" Property="Position">
          <PointFrame FrameIndex="0" X="832.0000" Y="568.0000">
            <EasingData Type="26" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="480.0000" Y="568.0000">
            <EasingData Type="26" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="832.0000" Y="568.0000">
            <EasingData Type="26" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="396031759" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="1.0000" Y="1.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="1.0000" Y="1.0000">
            <EasingData Type="26" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="396031759" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="wordIn" StartIndex="0" EndIndex="30">
          <RenderColor A="255" R="220" G="220" B="220" />
        </AnimationInfo>
        <AnimationInfo Name="wordOut" StartIndex="30" EndIndex="60">
          <RenderColor A="255" R="188" G="143" B="143" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Layer" Tag="216" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="1136.0000" />
        <Children>
          <AbstractNodeData Name="game_dialogbg" ActionTag="-780306881" Tag="217" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" TopMargin="88.0000" BottomMargin="88.0000" Scale9Width="640" Scale9Height="960" ctype="ImageViewObjectData">
            <Size X="640.0000" Y="960.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="568.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="0.8451" />
            <FileData Type="Normal" Path="HelloWorld.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="node_originParent" ActionTag="1760187276" Tag="234" FrameEvent="leftWordInComplete" IconVisible="True" LeftMargin="160.0000" RightMargin="480.0000" TopMargin="568.0000" BottomMargin="568.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="node_origin1" ActionTag="-925625205" Tag="219" IconVisible="True" TopMargin="-273.7300" BottomMargin="273.7300" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="img_wordBgOrigin1" ActionTag="381482378" CallBackName="onSelectWord" Tag="237" IconVisible="False" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="-40.5000" BottomMargin="-40.5000" Scale9Width="240" Scale9Height="81" ctype="ImageViewObjectData">
                    <Size X="240.0000" Y="81.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="BaS31.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt_zhWord1" ActionTag="2052686254" Tag="41" IconVisible="False" LeftMargin="-72.0000" RightMargin="-72.0000" TopMargin="-18.0000" BottomMargin="-18.0000" FontSize="36" LabelText="胆子不小" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="144.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <OutlineColor A="255" R="139" G="105" B="20" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position Y="273.7300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="node_origin1_0" ActionTag="130744905" Tag="220" IconVisible="True" TopMargin="-138.8300" BottomMargin="138.8300" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="img_wordBgOrigin2" ActionTag="613279012" Tag="246" IconVisible="False" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="-40.5000" BottomMargin="-40.5000" Scale9Width="240" Scale9Height="81" ctype="ImageViewObjectData">
                    <Size X="240.0000" Y="81.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="BaS31.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt_zhWord2" ActionTag="-178811368" Tag="42" IconVisible="False" LeftMargin="-77.0000" RightMargin="-67.0000" TopMargin="-19.0000" BottomMargin="-17.0000" FontSize="36" LabelText="胆子不小" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="144.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="-5.0000" Y="1.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position Y="138.8300" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="node_origin1_1" ActionTag="785171563" Tag="221" IconVisible="True" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="img_wordBgOrigin3" ActionTag="1667744554" Tag="245" IconVisible="False" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="-40.5000" BottomMargin="-40.5000" Scale9Width="240" Scale9Height="81" ctype="ImageViewObjectData">
                    <Size X="240.0000" Y="81.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="BaS31.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt_zhWord3" ActionTag="-438417705" Tag="43" IconVisible="False" LeftMargin="-72.0000" RightMargin="-72.0000" TopMargin="-18.0000" BottomMargin="-18.0000" FontSize="36" LabelText="胆子不小" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="144.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="node_origin1_2" ActionTag="-2099596595" Tag="222" IconVisible="True" TopMargin="130.9900" BottomMargin="-130.9900" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="img_wordBgOrigin4" ActionTag="660763798" Tag="244" IconVisible="False" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="-40.5000" BottomMargin="-40.5000" Scale9Width="240" Scale9Height="81" ctype="ImageViewObjectData">
                    <Size X="240.0000" Y="81.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="BaS31.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt_zhWord4" ActionTag="-2129647511" Tag="44" IconVisible="False" LeftMargin="-72.0000" RightMargin="-72.0000" TopMargin="-18.0000" BottomMargin="-18.0000" FontSize="36" LabelText="胆子不小" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="144.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position Y="-130.9900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="node_origin1_3" ActionTag="-779019251" Tag="223" IconVisible="True" TopMargin="265.9000" BottomMargin="-265.9000" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="img_wordBgOrigin5" ActionTag="-1905107516" Tag="243" IconVisible="False" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="-40.5000" BottomMargin="-40.5000" Scale9Width="240" Scale9Height="81" ctype="ImageViewObjectData">
                    <Size X="240.0000" Y="81.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="BaS31.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt_zhWord5" ActionTag="-1030969370" Tag="45" IconVisible="False" LeftMargin="-72.0000" RightMargin="-72.0000" TopMargin="-18.0000" BottomMargin="-18.0000" FontSize="36" LabelText="胆子不小" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="144.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position Y="-265.9000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="160.0000" Y="568.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2500" Y="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="node_targetParent" ActionTag="396031759" Tag="235" IconVisible="True" LeftMargin="480.0000" RightMargin="160.0000" TopMargin="568.0000" BottomMargin="568.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="node_origin1_4" ActionTag="1999336824" Tag="229" IconVisible="True" LeftMargin="0.0089" RightMargin="-0.0089" TopMargin="-273.7334" BottomMargin="273.7334" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="img_wordBgTarget1" ActionTag="557404151" Tag="242" IconVisible="False" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="-40.5000" BottomMargin="-40.5000" Scale9Width="240" Scale9Height="81" ctype="ImageViewObjectData">
                    <Size X="240.0000" Y="81.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="BaS31.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt_enWord1" ActionTag="1400758182" Tag="46" IconVisible="False" LeftMargin="-72.0000" RightMargin="-72.0000" TopMargin="-18.0000" BottomMargin="-18.0000" FontSize="36" LabelText="胆子不小" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="144.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="0.0089" Y="273.7334" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="node_origin1_0_0" ActionTag="-373037975" Tag="230" IconVisible="True" LeftMargin="0.0089" RightMargin="-0.0089" TopMargin="-138.8258" BottomMargin="138.8258" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="img_wordBgTarget2" ActionTag="-1038398920" Tag="241" IconVisible="False" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="-40.5000" BottomMargin="-40.5000" Scale9Width="240" Scale9Height="81" ctype="ImageViewObjectData">
                    <Size X="240.0000" Y="81.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="BaS31.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt_enWord2" ActionTag="-1608162625" Tag="47" IconVisible="False" LeftMargin="-72.0000" RightMargin="-72.0000" TopMargin="-18.0000" BottomMargin="-18.0000" FontSize="36" LabelText="胆子不小" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="144.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="0.0089" Y="138.8258" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="node_origin1_1_0" ActionTag="179404633" Tag="231" IconVisible="True" LeftMargin="0.0089" RightMargin="-0.0089" TopMargin="-3.9196" BottomMargin="3.9196" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="img_wordBgTarget3" ActionTag="476092268" Tag="240" IconVisible="False" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="-40.5000" BottomMargin="-40.5000" Scale9Width="240" Scale9Height="81" ctype="ImageViewObjectData">
                    <Size X="240.0000" Y="81.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="BaS31.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt_enWord3" ActionTag="-326003077" Tag="48" IconVisible="False" LeftMargin="-72.0000" RightMargin="-72.0000" TopMargin="-18.0000" BottomMargin="-18.0000" FontSize="36" LabelText="胆子不小" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="144.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="0.0089" Y="3.9196" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="node_origin1_2_0" ActionTag="-1770592701" Tag="232" IconVisible="True" LeftMargin="0.0089" RightMargin="-0.0089" TopMargin="130.9885" BottomMargin="-130.9885" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="img_wordBgTarget4" ActionTag="-1278714226" Tag="239" IconVisible="False" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="-40.5000" BottomMargin="-40.5000" Scale9Width="240" Scale9Height="81" ctype="ImageViewObjectData">
                    <Size X="240.0000" Y="81.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="BaS31.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt_enWord4" ActionTag="-793184153" Tag="49" IconVisible="False" LeftMargin="-72.0000" RightMargin="-72.0000" TopMargin="-18.0000" BottomMargin="-18.0000" FontSize="36" LabelText="胆子不小" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="144.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="0.0089" Y="-130.9885" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="node_origin1_3_0" ActionTag="966553577" Tag="233" IconVisible="True" LeftMargin="0.0089" RightMargin="-0.0089" TopMargin="265.8955" BottomMargin="-265.8955" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="img_wordBgTarget5" ActionTag="-897616107" Tag="238" IconVisible="False" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="-40.5000" BottomMargin="-40.5000" Scale9Width="240" Scale9Height="81" ctype="ImageViewObjectData">
                    <Size X="240.0000" Y="81.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="BaS31.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="txt_enWord5" ActionTag="401494789" Tag="50" IconVisible="False" LeftMargin="-72.0000" RightMargin="-72.0000" TopMargin="-18.0000" BottomMargin="-18.0000" FontSize="36" LabelText="胆子不小" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="144.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="0" G="0" B="0" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="0.0089" Y="-265.8955" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="480.0000" Y="568.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7500" Y="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>