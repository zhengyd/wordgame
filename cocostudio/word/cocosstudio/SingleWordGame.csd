<GameFile>
  <PropertyGroup Name="SingleWordGame" Type="Layer" ID="b331ef33-2977-4376-b4d8-257af0468e1b" Version="2.3.3.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="10" Speed="1.0000" ActivedAnimationName="showWord">
        <Timeline ActionTag="1574927586" Property="Position">
          <PointFrame FrameIndex="0" X="314.4960" Y="866.0864">
            <EasingData Type="26" />
          </PointFrame>
          <PointFrame FrameIndex="10" X="314.4960" Y="866.0864">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="1574927586" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.0100" Y="0.0100">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="10" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1574927586" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="10" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="showWord" StartIndex="0" EndIndex="10">
          <RenderColor A="255" R="211" G="211" B="211" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Layer" Tag="46" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="1136.0000" />
        <Children>
          <AbstractNodeData Name="SingleWordBg" ActionTag="1963867054" Tag="47" IconVisible="False" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="297.0000" RightMargin="297.0000" TopMargin="545.0000" BottomMargin="545.0000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="1136.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="568.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.0719" Y="0.0405" />
            <FileData Type="Normal" Path="arenaBackGround.jpg" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Node_1" ActionTag="1574927586" Tag="49" IconVisible="True" HorizontalEdge="BothEdge" VerticalEdge="BothEdge" LeftMargin="314.4960" RightMargin="325.5040" TopMargin="269.9136" BottomMargin="866.0864" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="-1677793840" Tag="50" IconVisible="False" HorizontalEdge="BothEdge" LeftMargin="-120.0000" RightMargin="-120.0000" TopMargin="-40.5004" BottomMargin="-40.4996" Scale9Width="240" Scale9Height="81" ctype="ImageViewObjectData">
                <Size X="240.0000" Y="81.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position Y="0.0004" />
                <Scale ScaleX="2.3076" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="BaS31.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="txt_originWord" ActionTag="1929887890" Tag="48" IconVisible="False" HorizontalEdge="BothEdge" LeftMargin="-36.0000" RightMargin="-36.0000" TopMargin="-18.0000" BottomMargin="-18.0000" FontSize="36" LabelText="生活" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="72.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="314.4960" Y="866.0864" />
            <Scale ScaleX="0.0100" ScaleY="0.0100" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4914" Y="0.7624" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_choose1" ActionTag="-1086480814" CallBackType="Click" CallBackName="menuCallback" Tag="51" IconVisible="False" LeftMargin="49.4071" RightMargin="350.5929" TopMargin="648.5962" BottomMargin="406.4038" TouchEnable="True" FontSize="36" ButtonText="设置" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="210" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="240.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="169.4071" Y="446.9038" />
            <Scale ScaleX="1.1133" ScaleY="1.2606" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2647" Y="0.3934" />
            <PreSize X="0.3750" Y="0.0713" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Normal" Path="BaS32.png" Plist="" />
            <PressedFileData Type="Normal" Path="BaS31.png" Plist="" />
            <NormalFileData Type="Normal" Path="BaS32.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_choose2" ActionTag="-1170235164" CallBackType="Click" CallBackName="menuCallback" Tag="52" IconVisible="False" LeftMargin="372.8618" RightMargin="27.1382" TopMargin="641.8786" BottomMargin="413.1214" TouchEnable="True" FontSize="36" ButtonText="设置" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="210" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="240.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="492.8618" Y="453.6214" />
            <Scale ScaleX="1.1133" ScaleY="1.2606" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7701" Y="0.3993" />
            <PreSize X="0.3750" Y="0.0713" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Normal" Path="BaS32.png" Plist="" />
            <PressedFileData Type="Normal" Path="BaS31.png" Plist="" />
            <NormalFileData Type="Normal" Path="BaS32.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_choose3" ActionTag="-1586130696" CallBackType="Click" CallBackName="menuCallback" Tag="53" IconVisible="False" LeftMargin="51.0487" RightMargin="348.9513" TopMargin="921.8979" BottomMargin="133.1021" TouchEnable="True" FontSize="36" ButtonText="设置" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="210" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="240.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="171.0487" Y="173.6021" />
            <Scale ScaleX="1.1133" ScaleY="1.2606" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2673" Y="0.1528" />
            <PreSize X="0.3750" Y="0.0713" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Normal" Path="BaS32.png" Plist="" />
            <PressedFileData Type="Normal" Path="BaS31.png" Plist="" />
            <NormalFileData Type="Normal" Path="BaS32.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_choose4" ActionTag="-1933338473" CallBackType="Click" CallBackName="menuCallback" Tag="54" IconVisible="False" LeftMargin="374.9529" RightMargin="25.0471" TopMargin="919.8082" BottomMargin="135.1918" TouchEnable="True" FontSize="36" ButtonText="设置" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="210" Scale9Height="59" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="240.0000" Y="81.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="494.9529" Y="175.6918" />
            <Scale ScaleX="1.1133" ScaleY="1.2606" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7734" Y="0.1547" />
            <PreSize X="0.3750" Y="0.0713" />
            <TextColor A="255" R="0" G="0" B="0" />
            <DisabledFileData Type="Normal" Path="BaS32.png" Plist="" />
            <PressedFileData Type="Normal" Path="BaS31.png" Plist="" />
            <NormalFileData Type="Normal" Path="BaS32.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bar_timeLeft" ActionTag="1129457478" Tag="56" IconVisible="False" LeftMargin="120.0000" RightMargin="120.0000" TopMargin="76.1442" BottomMargin="1045.8558" ctype="LoadingBarObjectData">
            <Size X="400.0000" Y="14.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="1052.8558" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9268" />
            <PreSize X="0.6250" Y="0.0123" />
            <ImageFileData Type="Default" Path="Default/LoadingBarFile.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="-411726938" Tag="58" IconVisible="False" LeftMargin="149.0712" RightMargin="166.9288" TopMargin="115.9370" BottomMargin="948.0630" FontSize="72" LabelText="时间3：00" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="324.0000" Y="72.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="311.0712" Y="984.0630" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4860" Y="0.8663" />
            <PreSize X="0.5063" Y="0.0634" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>