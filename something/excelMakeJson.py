import os
import sys, getopt
import xlrd
import json
import re

def readDirectoryWithExcel(sourceDir):
    files = [f for f in os.listdir(sourceDir) if re.match(r"\S\w+\.xlsx",f)]
    print files
    for file in files:
        print "file name is %s " % (file)
        file = sourceDir +'/'+ file
        handleExcel(file)
            
def handleExcel(fileName):
    book = xlrd.open_workbook(fileName)
    worksheet_names = book.sheet_names()
    for sheet_item_name in worksheet_names:
        converySheetToJson(book,sheet_item_name)
        
        
def converySheetToJson(book,sheet_name):
    sheet = book.sheet_by_name(sheet_name)
    masterlist = {}
    print sheet.ncols;
    print sheet.nrows;
    for row in range(1,sheet.nrows):        
        name = sheet.cell_value(row,0)
        name = name[2:]
        masterlist[name] = {}
        for col in range(1,sheet.ncols):
            key = sheet.cell_value(0,col)
            print("row = %f", row)
            value = sheet.cell_value(row,col)
            print(value)
            if key == "" or value == "":
                continue
            masterlist[name][key] = value

        
    #create JSON file using filename
    JSONname = sheet_name + '.json'
    fileName = output_file+'/'+JSONname;
    print '%s will be created!' % fileName
    with open(fileName, mode='w') as f:
      json.dump(masterlist, f)
    

if __name__ == "__main__":
    opts, args = getopt.getopt(sys.argv[1:], "hi:o:")
    input_file=""
    output_file=""
    for op, value in opts:
        if op == "-i":
            input_file = value
        elif op == "-o":
            output_file = value
        elif op == "-h":
            print 'useage: python excelToJson.py -i sourcedDirectory -o targetDirectory'
	print input_file
	print output_file
    handleExcel(input_file)

    # if input_file!="" and output_file!="":
    #     print("start to export")
    #     readDirectoryWithExcel(input_file);
	
    
	
