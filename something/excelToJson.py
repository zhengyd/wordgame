import sys
import xlrd
import codecs
import json


def exportExcelToJson(fileName):
	workbook = xlrd.open_workbook(fileName)
	sheet = workbook.sheet_by_name("Sheet1")
	zhlocalizationObj = {}
	enlocalizationObj = {}
	jsonData = {}
	print "onParse"
	i = 1
	for row in xrange(0,sheet.nrows,1):
		zhValue = sheet.cell_value(row,0)
		#print zhValue
		enValue = sheet.cell_value(row,1)
		#print enValue
		if enValue != "":
			enValueKey = enValue.replace(" ","")
			enlocalizationObj[i] = enValue
			zhlocalizationObj[i] = zhValue

			#jsonData[enValueKey] = zhValue
		else:
			print zhValue
		i = i + 1
		#for col in xrange(0,sheet.ncols,1):
			#print col

			#zhValue = sheet.cell_value(row,col - 1)
			#print "zh ++++++++++ "+zhValue + "\n"
			#enValue = sheet.cell_value(row + 1,col)
			#print "en ++++++++++ "+enValue + "\n"
			# enValueItemKey = enValue.replace(" ","")
			# enlocalizationObj[enValueItemKey] = enValue
			# zhlocalizationObj[enValueItemKey] = zhValue

			# jsonData[enValueKey].append(enValueItemKey)

	return jsonData,enlocalizationObj,zhlocalizationObj

	pass
def writeFile(jsonData, outFileName):
	with codecs.open(outFileName,"w","utf-8") as f:
		json.dump(jsonData,f,ensure_ascii= False)

def main(fileName):
	jsonData , enData, zhData = exportExcelToJson(fileName)
	#writeFile(jsonData,fileName+".json")
	fileName = fileName.replace(".xls","")
	writeFile(enData,"en_"+fileName+".properties")
	writeFile(zhData,"zh_"+fileName+".properties")

if __name__ == '__main__':
	main(sys.argv[1])