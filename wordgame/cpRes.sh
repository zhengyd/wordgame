cd .
echo "data"
cp ../something/en_English.json ./res/data
cp ../something/zh_English.json ./res/data

echo "rm anroid assets...."
rm -rf ./frameworks/runtime-src/proj.android/assets/*
echo "cp res................"
cp -r ./res ./frameworks/runtime-src/proj.android/assets
echo "cp src................."
cp -r ./src ./frameworks/runtime-src/proj.android/assets
cp ./config.json ./frameworks/runtime-src/proj.android/assets
cd ./frameworks/runtime-src/proj.android/
 ant clean
 echo "-----------------------------------------------------------------------------"
 ant release

 echo "complete"

 if [[ $1==1 ]]; then
	 echo "install to devices----------------------------"
	 cd ./bin
	 ls | grep "wordgame-release.apk$" | xargs adb install -r
	 cd ../
 fi

