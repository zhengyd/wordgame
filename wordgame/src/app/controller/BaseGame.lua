
	local BaseGame = class("BaseGame")

	function BaseGame:ctor( ... )
		-- body
	end

	function BaseGame:init(gameData)
		-- body
	end

	function BaseGame:update( ... )
		-- body
	end

	function BaseGame:updateWithNewData( gameData )
		-- body
	end

	function BaseGame:delete(  )
		-- body
		---clean self
	end

	return BaseGame