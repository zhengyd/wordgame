
	local GameModeFty = class("GameModeFty")

	function GameModeFty:createGameWithMode( mode )
		-- body
		if mode == GAME_MODE_SINGLE then
			return require("SingleGame").new()
		elseif mode == GAME_MODE_MULTIPLE then
			return require("MultiGame").new()
		else
			return require("BaseGame").new()
		end
	end

	return GameModeFty