
	local parent = import(".BaseGame")
	local SingleGame = class("SingleGame",parent)

	function SingleGame:ctor( ... )
		-- body
	end

	function  SingleGame:init( gameData )
		SingleGame.super.init(self,gameData)
		-- body
	end

	function SingleGame:delete( ... )
		SingleGame.super.delete(self)
		-- body
	end

	return SingleGame