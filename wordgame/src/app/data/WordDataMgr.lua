	local WordDataMgr = class("WordDataMgr")
	function  WordDataMgr:ctor( ... )
		-- body
		-- self.mLearnData learn data from net or local store
		-- self.mConfigLevelData @ the data of config 
	end

	function WordDataMgr:initData(enData,zhData )
		-- body
		--- load learn data from net or local store
		self.mEnData = enData
		self.mZhData = zhData
		self.mCounts = table.nums(self.mZhData)
		print(self.mCounts,"self.mCountsself.mCountsself.mCountsself.mCounts")
	end

	function  WordDataMgr:randomData( needWordNumber )
		-- body
		needWordNumber = needWordNumber or 1

	end

	--@getdata by asc
	function WordDataMgr:getShowDataByNumber( needWordNumber)
		-- body
		needWordNumber = needWordNumber or 1
		self.mCursor = self.mCursor or 1

		local index = 1
		local currentZhData = {}
		local currentEnData = {}
		for _index = 1, needWordNumber do
			if self.mCursor >= self.mCounts then
				self.mCursor = 1
			end
			currentEnData[tostring(self.mCursor)] = self.mEnData[tostring(self.mCursor)]
			currentZhData[tostring(self.mCursor)] = self.mZhData[tostring(self.mCursor)]
			self.mCursor = self.mCursor + 1
		end
		dump(currentEnData,"currentEnData")
		dump(currentZhData,"currentEnData")
		return currentEnData , currentZhData
	end

	-- get then class of learn by the level 
	-- @level primary,junior hight,senior hight, then four ,the six and Tofel so on
	function WordDataMgr:getCurrentClassByLevel( level )
		-- body
		if self.mLearnData and self.mLearnData[level] then
			return self.mLearnData[level].currentClass or 1
		end

		return 1
	end

	function WordDataMgr:setCurrentLevel( level )
		-- body
		self.mCurrentLevel = level or 1
		---and release last level data and load current level
	end

	-- this func used for load config data
	function WordDataMgr:getClassDataByIndex( classIndex, level )
		-- body
		local level = level or self.mCurrentLevel or 1
		if self.mConfigLevelData and self.mConfigLevelData[level] then
			return self.mConfigLevelData[level]
		end
		print("the level data is not loaded "..tostring(level))
		return {}
	end

	function WordDataMgr:onCompleteOneClass( classIndex, level )
		-- body
		-- save local ant net
		-- update data
		local level = level or self.mCurrentLevel
		if level and self.mLearnData and self.mLearnData[level] then
			local configData = self.mConfigLevelData[level] or {}
			if configData[classIndex + 1 ] then
				self.mLearnData[level].currentClass = classIndex + 1
			else
				---complete this level
				self.mLearnData[level].status = COMPLETE
			end
			
		end
	end
	
	return WordDataMgr