	

	local Unit = class("Unit")
	
	function Unit:seekNodeByName( root, targetNodeName )
		-- body
		if not root then
			return nil
		end
		if root:getName() == targetNodeName then
			return root
		end
	    local arrayRootChildren = root:getChildren() or {}

	    for _index, child in pairs(arrayRootChildren) do
	    	local targetNode = self:seekNodeByName(child, targetNodeName)
	    	if targetNode then
	    		return targetNode
	    	end
	    end
	    return nil
	end


	function Unit:seekNodeByTag( root, targetTag )
		-- body
		if not root then
			return nil
		end
		if root:getTag() == targetTag then
			return root
		end
	    local arrayRootChildren = root:getChildren() or {}

	    for _index, child in pairs(arrayRootChildren) do
	    	local targetNode = self:seekNodeByTag(child, targetNodeName)
	    	if targetNode then
	    		return targetNode
	    	end
	    end
	    return nil
	end

	return Unit