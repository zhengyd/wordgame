
	-- this class is the base class of UI
	local BaseLayer = class("BaseLayer")

	function BaseLayer:ctor( name, node ,actionLine)
		-- bod
		self.mDialog = node
		self.mName = name
		self.mActionLine = actionLine
	end

	function BaseLayer:init( ... )
		-- body
	end

	function BaseLayer:update( ... )
	 	-- body

	end

	function BaseLayer:updateWithNewData(newData)
		self.mData = newData
	end

	function BaseLayer:show()

	end

	function BaseLayer:getNode()
		return self.mDialog
	end

	function BaseLayer:close()
		self.mDialog:removeFromParentAndCleanup()
		self.mDialog = nil
		self.mName = nil
		self.mActionLine = nil
	end

	return BaseLayer