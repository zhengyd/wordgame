	local p = import(".BaseLayer")
	local MainFrameLayer = class("MainFrameLayer",p)

	function MainFrameLayer:init( )
		-- body
		--- create MainMenu
		self.mDialogQueques = {}
		local node ,layer= UIManager:create("MainMenu")
		self.mCurrentDialog = layer
		self.mDialog:addChild(node,LAYER_GAME,LAYER_GAME)
		self.mCurrentDialog:init()
		table.insert(self.mDialogQueques,"MainMenu")
	end

	function MainFrameLayer:showDialogWithName(dialogName, animation )
		-- body
		local node ,layer= UIManager:create(dialogName)
		if node then
			local originNode = self.mDialog:getChildByTag(LAYER_GAME)
			if originNode then
				originNode:removeFromParent()
				originNode = nil
			end
			self.mDialog:addChild(node,LAYER_GAME)
			self.mCurrentDialog = layer
			self.mCurrentDialog:init()
			table.insert(self.mDialogQueques,dialogName)
		end
	end

	function MainFrameLayer:onBackDialog( ... )
		-- body
		if self.mDialogQueques then
			---remove current dialog and show stack dialog
			local dialogNumber = table.nums(self.mDialogQueques)
			if dialogNumber > 0 then
				table.remove(self.mDialogQueques, dialogNumber)
			end
			local currentDialog = self.mDialogQueques[#self.mDialogQueques]

			local node ,layer= UIManager:create(dialogName)
			local originNode = self.mDialog:getChildByTag(LAYER_GAME)
			if originNode then
				self.mDialog:removeFromParent()
			end
			self.mDialog:addChild(node,LAYER_GAME,LAYER_GAME)
			self.mCurrentDialog = layer

		end
	end

	function MainFrameLayer:onCreate( ... )
		
	end

	return MainFrameLayer