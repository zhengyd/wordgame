	local p = import(".BaseLayer")
	local MainMenuLayer = class("MainMenuLayer",p)

	function MainMenuLayer:init( ... )
		-- body
		if self.mUIActionLine then
			self.mUIActionLine:play("show",false)
		end
		
		self.mUIActionLine = cc.CSLoader:createTimeline("MainMenu.csb")
        self.mDialog:runAction(self.mUIActionLine)
        self.mUIActionLine:play("show",false)

        local parcticeMode =  Unit:seekNodeByName(self.mDialog, "btn_ParcticeMode")
        parcticeMode:addClickEventListener(handler(self,self.menuCallback))
        local parcticeMode =  Unit:seekNodeByName(self.mDialog, "btn_NoEndMode")
        parcticeMode:addClickEventListener(handler(self,self.menuCallback))
	end

	function MainMenuLayer:update( ... )
		-- body
	end

	function MainMenuLayer:menuCallback(sender, eventType )
		-- body
	    dump(sender,"sendersendersendersendersendersendersendersendersendersendersender")
        local nodeName = sender:getName()
        print("---------++++++++++"..tostring(nodeName))
        if nodeName == "btn_ParcticeMode" then
        	UIManager:showDialog("SingleWordGame")
        elseif nodeName == "btn_NoEndMode" then
        	UIManager:showDialog("MutilWordGame")
        end
	end
	return MainMenuLayer