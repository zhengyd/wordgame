
local MainScene = class("MainScene", cc.load("mvc").ViewBase)

function MainScene:onCreate()
    -- add background image
    -- display.newSprite("HelloWorld.png")
    --     :move(display.center)
    --     :addTo(self)

    -- add HelloWorld label
 --    cc.Label:createWithSystemFont("Hello World", "Arial", 40)
 --        :move(display.cx, display.cy + 200)
 --        :addTo(self)
 --        dump(self,"sssssssssssssssssssssssssssssssssssssssss")
	--     --local layer =  ccs.GUIReader:getInstance():widgetFromBinaryFile("res/MainScene.csb")
 --    local zhData = FileParse:loadFile("data/zh_English.json")
 --    local enData = FileParse:loadFile("data/en_English.json")
 --    dump(zhData," zhDa-------------------------")
	-- WordDataMgr:initData(enData,zhData)
 --    self:createUILayer()
 --    self:createMenuLayer()
 --    self:playMusic()
    self:createMainView()
     	    
end

function MainScene:createMainView( ... )
    -- body
    local node, nodeController = UIManager:create("MainFrame")
    UIManager:setDelegate(nodeController)
    self:addChild(node)
end

function MainScene:fillWordData()
    -- body
    local showZhData, showEnData = WordDataMgr:getShowDataByNumber(5)
    local originParent = self.mUILayer:getChildByName("node_originParent")
    local targetParent = self.mUILayer:getChildByName("node_targetParent")
    local _index = 1
    for _, value in pairs(showZhData) do
        local txt_zhWord = Unit:seekNodeByName(originParent, "txt_zhWord".._index)
        local txt_enWord = Unit:seekNodeByName(targetParent, "txt_enWord".._index)
        if txt_zhWord then
            txt_zhWord:setString(value)
        end
        if txt_enWord then
            txt_enWord:setString(showEnData[_])
        end
        _index = _index + 1
    end
end

function MainScene:createUILayer( ... )
    -- body
    local layer = cc.CSLoader:createNode("UILayer.csb")
        dump(layer,"sssssssssssssssssssssssssssssssssssssssss")
        self.mUIActionLine = cc.CSLoader:createTimeline("UILayer.csb")
        layer:runAction(self.mUIActionLine)
        --actionLine:gotoFrameAndPlay(0,30,false)
        self.mUIActionLine:play("wordIn",false)
        self:addChild(layer)
        self.mUILayer = layer
        self:fillWordData()

        local function onFrameEventTrigger( frame )
            -- body
            print("onFrameEventTriggeronFrameEventTrigger\n")
            local eventName = frame:getEvent()
             print("\n----onFrameEventTriggeronFrameEventTrigger +++ "..tostring(eventName))
            if eventName == "leftWordInComplete" then

            elseif eventName == "leftWordOutComplete" then
                print("+++++leftWordOutCompleteleftWordOutComplete -----"..tostring(eventName))
                self:fillWordData()
                self.mUIActionLine:play("wordIn",false)
            end
        end
        self.mUIActionLine:setFrameEventCallFunc(onFrameEventTrigger)
end

function MainScene:createMenuLayer( ... )
    -- body

        local layer = cc.CSLoader:createNode("MenuLayer.csb")
        self:addChild(layer)
        self.mMenuLayer = layer
        local playOutBtn = layer:getChildByName("Button_1")

        local stopBtn =  Unit:seekNodeByName(layer, "music_stop")
        local nextPlayBtn =  Unit:seekNodeByName(layer, "music_next")
        -- local playInBtn = ccui.Helper:seekWidgetByName(layer, "Button_1_2")
        -- dump(playOutBtn,"manshendeechou!!!!!!!!!!!!!!!!!!!!!!!")
        --  dump(playInBtn,"manshendeechou!!!!!!!!!!!!!!!!!!!!!!!")
        local function menuCallback(sender, eventType)
            dump(sender,"sendersendersendersendersendersendersendersendersendersendersender")
            --local btnName = sender.getTag()
            --dump(btnName,"sendersenbtnNamebtnNamebtnNamebtnNamebtnNamer")
            local nodeName = sender:getName()
            print("---------++++++++++"..tostring(nodeName))
            if nodeName == "Button_1" then
                self.mUIActionLine:play("wordOut",false)
            elseif nodeName == "music_stop" then
                audio.stopMusic()
            elseif nodeName == "music_next" then
                self:playMusic()
            end
            
        end
        --playOutBtn:setButtonLabelString("翻页")
        playOutBtn:addClickEventListener(menuCallback)
        local stopBtn = layer:getChildByName("music_stop")
        --playOutBtn:setButtonLabelString("翻页")
        stopBtn:addClickEventListener(menuCallback)
        --playOutBtn:setButtonLabelString("翻页")
        nextPlayBtn:addClickEventListener(menuCallback)

end

    function MainScene:playMusic( ... )
        -- body
        local musics = {"music/LifeIsWonderful.mp3","music/SeeYouAgain.mp3"}
        local roundNumber = math.random(table.nums(musics))
        audio.playMusic(musics[roundNumber],true)
    end


return MainScene
