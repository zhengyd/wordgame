
	local parent = import(".BaseLayer")
	local MutilWordGameLayer = class("MutilWordGameLayer", parent)

	function MutilWordGameLayer:init( ... )
		self:loadUI()
        self:loadData()
        self:startGame()
	end

    function MutilWordGameLayer:loadData( ... )
        local zhData = FileParse:loadFile("data/zh_English.json")
        local enData = FileParse:loadFile("data/en_English.json")
        --dump(zhData," zhDa-------------------------")
        WordDataMgr:initData(enData,zhData)
    end

	function MutilWordGameLayer:startGame( ... )
		-- body
		self:fillWordData()

		local function onFrameEventTrigger( frame )
            -- body
            print("onFrameEventTriggeronFrameEventTrigger\n")
            local eventName = frame:getEvent()
             print("\n----onFrameEventTriggeronFrameEventTrigger +++ "..tostring(eventName))
            if eventName == "leftWordInComplete" then

            elseif eventName == "leftWordOutComplete" then
                print("+++++leftWordOutCompleteleftWordOutComplete -----"..tostring(eventName))
                self:fillWordData()
                self.mUIActionLine:play("wordIn",false)
            end
        end
        self.mUIActionLine:setFrameEventCallFunc(onFrameEventTrigger)
	end

	function MutilWordGameLayer:loadUI( ... )
		-- body
		if self.mUIActionLine then
			self.mUIActionLine:play("wordIn",false)
		else
			self.mUIActionLine = cc.CSLoader:createTimeline("MutilWordGame.csb")
			self.mDialog:runAction(self.mUIActionLine)
			self.mUIActionLine:play("wordIn",false)
		end

        ---添加touch事件监听
        local originParent = self.mDialog:getChildByName("node_originParent")
        local targetParent = self.mDialog:getChildByName("node_targetParent")

        for i = 1, 5 do
            local imgRender = Unit:seekNodeByName(self.mDialog, "img_wordBgOrigin"..i)
            imgRender:setTouchEnabled(true)
            imgRender:addClickEventListener(handler(self, self.onClickWord))
            local imgRender = Unit:seekNodeByName(self.mDialog,"img_wordBgTarget"..i)
            imgRender:addClickEventListener(handler(self, self.onClickWord))
            imgRender:setTouchEnabled(true)
        end
    end

    function MutilWordGameLayer:onClickWord(render)
        local renderName = render:getName()
        print("======================================= "..tostring(renderName))
        local touchIndex = string.sub(renderName,17)
        if renderName == self.mSelectedRenderName then

        end
        if self.mSelectedRenderName then
            ---点击的全是中文 或者全是英文
            if string.sub(self.mSelectedRenderName,1,16) == string.sub(renderName,1,16) then
                self:resetWordSelected(self.mSelectedRenderName)
                self.mSelectedRenderName = nil
            elseif self:checkCanDisappear(renderName) then
                self:disappearCouple(renderName)
                if self:checkNeedNext() then
                    self:next()
                end
            else
                self:resetWordSelected(self.mSelectedRenderName)
                self.mSelectedRenderName = nil
            end
        else
            self:setWordSelected(renderName)
        end
    end

    function MutilWordGameLayer:resetWordSelected(selectedRenderName)
        local node = Unit:seekNodeByName(self.mDialog, selectedRenderName)
        if node:getChildByTag(100) then
            node:removeChildByTag(100)
        end
    end

    function MutilWordGameLayer:setWordSelected( renderName )
        local node = Unit:seekNodeByName(self.mDialog, renderName)
        if node then
            if node:getChildByTag(100) then
                node:removeChildByTag(100)
            end

            local selectedSprite = display.newSprite("border.png")
            selectedSprite:setAnchorPoint(0,0)
            node:addChild(selectedSprite,10,100)
            self.mSelectedRenderName = renderName
        end
    end

    ---检查是否能消掉
    function MutilWordGameLayer:checkCanDisappear(renderName)
        dump(self.mWordMaps)
        if self.mWordMaps and self.mSelectedRenderName and renderName then
            local selectedIndex = string.sub(self.mSelectedRenderName,17)
            local touchIndex = string.sub(renderName,17)
            local renderNamePre = string.sub(self.mSelectedRenderName,1,16)
            print("touchIndex  "..touchIndex.." "..selectedIndex)
            if renderNamePre == "img_wordBgOrigin" then
                return self.mWordMaps[checknumber(selectedIndex)] == checknumber(touchIndex)
            else
                return self.mWordMaps[checknumber(touchIndex)] == checknumber(selectedIndex)
            end
        end
    end

    function MutilWordGameLayer:disappearCouple( renderName )
        if self.mSelectedRenderName then
            local node = Unit:seekNodeByName(self.mDialog, self.mSelectedRenderName)
            node:getParent():setVisible(false)
            self:resetWordSelected(self.mSelectedRenderName)
        end

        if renderName then
            local node = Unit:seekNodeByName(self.mDialog, renderName)
            node:getParent():setVisible(false)
            self:resetWordSelected(renderName)
        end

        if not self.mDisappearIndex then
            self.mDisappearIndex = {}
        end

        self.mDisappearIndex[self.mSelectedRenderName] = renderName

        self.mSelectedRenderName = nil
    end

    function MutilWordGameLayer:checkNeedNext( ... )
        if self.mDisappearIndex and table.nums(self.mDisappearIndex) >= 5 then
            return true
        end
    end
    ---开始下一轮
    function MutilWordGameLayer:next()
        self.mDisappearIndex = {}
        self.mUIActionLine:play("wordOut",false)
    end

	function MutilWordGameLayer:fillWordData()
	    -- body
        self:resetUI()
        self.mWordMaps = {}
	    local showEnData, showZhData = WordDataMgr:getShowDataByNumber(5)
	    local originParent = self.mDialog:getChildByName("node_originParent")
	    local targetParent = self.mDialog:getChildByName("node_targetParent")

        local keyZhIndexs = table.keys(showZhData)
        local keyEnIndexs = table.keys(showEnData)
        local enIndexs = {}
        local zhIndexs = {}
        for index = 5, 1, -1 do
            local randomIndex = math.random(1, index)
            local keyIndex = table.remove(keyZhIndexs, randomIndex)
            table.insert(enIndexs, keyIndex)

            local randomIndex = math.random(1, index)
            local keyIndex = table.remove(keyEnIndexs, randomIndex)
            table.insert(zhIndexs, keyIndex)
        end
        dump(enIndexs)
        dump(zhIndexs)

	    for _index, value in pairs(zhIndexs) do
            local enIndex = table.keyof(enIndexs, value)
            self.mWordMaps[_index] = enIndex
	    end

        for _index = 1, 5 do
            local txt_zhWord = Unit:seekNodeByName(originParent, "txt_zhWord".._index)
            local txt_enWord = Unit:seekNodeByName(targetParent, "txt_enWord".._index)
            if txt_zhWord then
                local value = showZhData[zhIndexs[_index]]
                txt_zhWord:setString(value)
            end
            if txt_enWord then
                local value = showEnData[enIndexs[_index]]
                txt_enWord:setString(value)
            end
        end
        dump(self.mWordMaps)
	end

    function MutilWordGameLayer:resetUI( ... )
        for i = 1, 5 do
            local nodeName = "img_wordBgOrigin"..i
            local node = Unit:seekNodeByName(self.mDialog, "img_wordBgOrigin"..i)
            node:getParent():setVisible(true)
            self:resetWordSelected(nodeName)

            nodeName = "img_wordBgTarget"..i
            local node = Unit:seekNodeByName(self.mDialog, "img_wordBgTarget"..i)
            node:getParent():setVisible(true)
            self:resetWordSelected(nodeName)
        end
    end

	return MutilWordGameLayer