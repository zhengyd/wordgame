
	---this class is the Manager of UI
	local UIManager = class("UIManager")

	function UIManager:ctor( ... )
		-- body
		self.mViewRoot = "app.views"
	end
	function UIManager:create( name )
	 	-- body
	 	-- check CSB resource file
	    return self:createResoueceNode(name)

	end

	function UIManager:createResoueceNode(name)
		local isFileExist =FileUtils:isFileExist(name..".csb")
		if not isFileExist then
			reportError("the file is not found with name "..name..".csb")
			return
		end
	    local node = cc.CSLoader:createNode(name..".csb")
	    local actionLine = cc.CSLoader:createTimeline(name..".csb")
        local packageName = string.format("%s.%s", self.mViewRoot, name..'Layer')
        local status, view = xpcall(function()
               							return require(packageName).new(name,node,actionLine)
            						end,
            		function(msg)
			            if not string.find(msg, string.format("'%s' not found:", packageName)) then
			                print("load view error: ", msg)
			            end
        			end)
			        local t = type(view)
			        if status and (t == "table" or t == "userdata") then
			            view:init()
			        end
		return node, view
	end

	function UIManager:setDelegate( controller )
		-- body
		self.mDeleagte = controller
	end

	function UIManager:showDialog( dialogName )
		-- body
		if self.mDeleagte then
			self.mDeleagte:showDialogWithName(dialogName)
		end
	end

	function UIManager:popDialogWithName( dialogName )
		-- body
	end
	
	return UIManager