
local ViewBase = class("ViewBase", cc.Node)

function ViewBase:ctor(app, name)
    self:enableNodeEvents()
    self.app_ = app
    self.name_ = name

    -- check CSB resource file
    local res = rawget(self.class, "RESOURCE_FILENAME")
    if res then
        self:createResoueceNode(res)
    end

    local binding = rawget(self.class, "RESOURCE_BINDING")
    if res and binding then
        self:createResoueceBinding(binding)
    end

    if self.onCreate then self:onCreate() end
end

function ViewBase:getApp()
    return self.app_
end

function ViewBase:getName()
    return self.name_
end

function ViewBase:getResourceNode()
    return self.resourceNode_
end

function ViewBase:createResoueceNode(resourceFilename)
    if self.resourceNode_ then
        self.resourceNode_:removeSelf()
        self.resourceNode_ = nil
    end
    self.resourceNode_ = cc.CSLoader:createNode(resourceFilename)
    assert(self.resourceNode_, string.format("ViewBase:createResoueceNode() - load resouce node from file \"%s\" failed", resourceFilename))
    self:addChild(self.resourceNode_)
end

function ViewBase:createResoueceBinding(binding)
    assert(self.resourceNode_, "ViewBase:createResoueceBinding() - not load resource node")
    for nodeName, nodeBinding in pairs(binding) do
        local node = self.resourceNode_:getChildByName(nodeName)
        if nodeBinding.varname then
            self[nodeBinding.varname] = node
        end
        for _, event in ipairs(nodeBinding.events or {}) do
            if event.event == "touch" then
                node:onTouch(handler(self, self[event.method]))
            end
        end
    end
end

function ViewBase:showWithScene(transition, time, more)
    self:setVisible(true)
    local scene = display.newScene(self.name_)

    local director = cc.Director:getInstance()
    local view = director:getOpenGLView()
    local rsSize = view:getDesignResolutionSize()

    print("display width is "..display.size.width)
    print(display.sizeInPixels.width)
    print(rsSize.width)
    local showY = (display.size.width - 640 ) / 2

    local border = display.newSprite("HelloWorld.png")
    local borderSize = border:getContentSize()
    local scaleX = showY / borderSize.width
    local scaleY = display.size.height / borderSize.height
    border:setScaleX(scaleX)
    border:setScaleY(scaleY)
    border:setPosition(showY/2, display.cy)
    scene:addChild(border)

    local border = display.newSprite("HelloWorld.png")
    local borderSize = border:getContentSize()
    local scaleX = showY / borderSize.width
    local scaleY = display.size.height / borderSize.height
    border:setScaleX(-scaleX)
    border:setScaleY(scaleY)
    border:setPosition(display.size.width - showY/2 , display.cy)
    scene:addChild(border)

    scene:addChild(self)
    print("showYshowYshowYshowY is "..showY)
    self:setPosition(showY, 0)

    display.runScene(scene, transition, time, more)
    return self
end

return ViewBase
